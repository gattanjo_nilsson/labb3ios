//
//  AddItemViewController.h
//  Labb3
//
//  Created by ITHS on 2016-02-09.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSXItem.h"

@interface AddItemViewController : UIViewController

@property () NSMutableArray *itemList;
@property () NSMutableArray *completedItemList;
@property NSString *descript;
@property NSString *priority;
@property BOOL *completed;
@property NSDate *deadline;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;


@end
