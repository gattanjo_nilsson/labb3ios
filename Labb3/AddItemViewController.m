//
//  AddItemViewController.m
//  Labb3
//
//  Created by ITHS on 2016-02-09.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "AddItemViewController.h"
#import "NSXItem.h"

@interface AddItemViewController ()
@property (weak, nonatomic) IBOutlet UITextField *descriptTextField;
@property (strong,nonatomic) NSXItem *NewItem;

@end

@implementation AddItemViewController

-(NSXItem*)NewItem {
    if(!_NewItem) {
        _NewItem = [[NSXItem alloc] init];
    }
    return _NewItem;
}

- (IBAction)prioritySegmentedControl:(UISegmentedControl *)sender {
    if(sender.selectedSegmentIndex == 0){
        self.priority = @"High";
    }
    else {
        self.priority = @"Low";
    }
}

- (IBAction)completedSegmentedControl:(UISegmentedControl *)sender {
    if(sender.selectedSegmentIndex == 0){
        self.completed = YES;
    }
    else {
        self.completed = NO;
    }
}

- (IBAction)deadlineDatePicker:(UIDatePicker *)sender {
    self.deadline = sender.date;
}

- (IBAction)addButton:(UIButton *)sender {
    self.NewItem.descript = self.descriptTextField.text;
    self.NewItem.priority = self.priority;
    self.NewItem.completed = self.completed;
    self.NewItem.deadline = self.deadline;
    if(self.completed == YES) {
        [self.completedItemList addObject:self.NewItem];
    }
    else {
        [self.itemList addObject:self.NewItem];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.priority = @"High";
    self.completed = YES;
    self.deadline = _datePicker.date;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
