//
//  CustomCell.h
//  Labb3
//
//  Created by ITHS on 2016-02-13.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *customDescriptLabel;
@property (strong, nonatomic) IBOutlet UILabel *customDeadlineLabel;
@property (strong, nonatomic) IBOutlet UILabel *customPriorityLabel;
@property (weak, nonatomic) IBOutlet UILabel *customCompletedLabel;

@end
