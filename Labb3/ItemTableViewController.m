//
//  ItemTableViewController.m
//  Labb3
//
//  Created by ITHS on 2016-02-09.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "ItemTableViewController.h"
#import "NSXItem.h"
#import "AddItemViewController.h"
#import "CustomCell.h"

@interface ItemTableViewController ()


@property(nonatomic) NSMutableArray *itemList;
@property(nonatomic) NSMutableArray *completedItemList;

@end

@implementation ItemTableViewController

-(NSMutableArray*)itemList {
    if (!_itemList) {
        self.itemList  = [[NSMutableArray alloc] init];
        
        NSXItem *item = [[NSXItem alloc] init];
        item.deadline = [NSDate date];
        item.descript = @"Give Gattanjo High Grades";
        item.priority = @"High";
        item.completed = NO;
        
        [self.itemList addObject:item];
    }
    return _itemList;
}

-(NSMutableArray*)comleptedItemList {
    if (!_completedItemList) {
        self.completedItemList  = [[NSMutableArray alloc] init];
        
        NSXItem *completedItem = [[NSXItem alloc] init];
        completedItem.deadline = [NSDate date];
        completedItem.descript = @"Finish Labb3";
        completedItem.priority = @"High";
        completedItem.completed = YES;
        
        [self.completedItemList addObject:completedItem];
    }
    return _completedItemList;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (section) {
        case 0:
            return self.itemList.count;
        default:
            return self.completedItemList.count;
            break;
    }
}

-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"Incomplete tasks";
            break;
            
        default:
            return @"Completed tasks";
            break;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSXItem *item = self.itemList[indexPath.row];
    NSXItem *completedItem = self.completedItemList[indexPath.row];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *date = [dateFormatter stringFromDate:item.deadline];
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd"];
    NSString *date2 = [dateFormatter2 stringFromDate:item.deadline];
    
   NSString * booleanString = (item.completed) ? @"True" : @"False";
    NSString * booleanString2 = (completedItem.completed) ? @"True" : @"False";
    
    
    switch (indexPath.section) {
        case 0:
            cell.customDescriptLabel.text = item.descript;
            cell.customDeadlineLabel.text = date;
            cell.customPriorityLabel.text = item.priority;
            cell.customCompletedLabel.text = booleanString;
            break;
            
        default:
            cell.customDescriptLabel.text = completedItem.descript;
            cell.customDeadlineLabel.text = date2;
            cell.customPriorityLabel.text = completedItem.priority;
            cell.customCompletedLabel.text = booleanString2;
            break;
    }
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.itemList removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    AddItemViewController *addItemViewController = [segue destinationViewController];
    addItemViewController.itemList = self.itemList;
}

@end
