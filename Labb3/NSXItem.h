//
//  NSXItem.h
//  Labb3
//
//  Created by ITHS on 2016-02-09.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSXItem : NSObject

@property NSString *descript;
@property NSString *priority;
@property BOOL *completed;
@property NSDate *deadline;

@end
