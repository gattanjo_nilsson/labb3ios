//
//  main.m
//  Labb3
//
//  Created by ITHS on 2016-02-09.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
